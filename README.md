## full_k62v1_64_bsp-user 11 RP1A.200720.011 1630918859 release-keys
- Manufacturer: ulefone
- Platform: mt6765
- Codename: Armor_X5_R
- Brand: Ulefone
- Flavor: full_k62v1_64_bsp-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1630918859
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Armor_X5_R/Armor_X5_R:11/RP1A.200720.011/1630918859:user/release-keys
- OTA version: 
- Branch: full_k62v1_64_bsp-user-11-RP1A.200720.011-1630918859-release-keys
- Repo: ulefone_armor_x5_r_dump_13854


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
